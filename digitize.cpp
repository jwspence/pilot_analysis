#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include "EdbDataSet.h"
#include "EdbSegP.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
using namespace std;
//-----------------------------------------------------------
Float_t CalcChi2(EdbSegP* s1, EdbSegP* s2, EdbScanCond* c)
{
  Float_t dZ = s2->Z() - s1->Z();
  Float_t TX = (s2->X() - s1->X())/dZ;
  Float_t TY = (s2->Y() - s1->Y())/dZ;
  Float_t dTX1 = s1->TX() - TX;
  Float_t dTX2 = s2->TX() - TX;
  Float_t dTY1 = s1->TY() - TY;
  Float_t dTY2 = s2->TY() - TY;
  Float_t restx = c->SigmaTX(TX);
  Float_t restx2 = restx*restx;
  Float_t resty = c->SigmaTY(TY);
  Float_t resty2 = resty*resty;
  Float_t chi2 = ((dTX1*dTX1)/restx2 + (dTX2*dTX2)/restx2 +
                  (dTY1*dTY1)/resty2 + (dTY2*dTY2)/resty2)/2;
  return chi2;
}
//-----------------------------------------------------------
Float_t Chi2P(EdbSegP* s1, EdbSegP* s2, EdbScanCond* c)
{
  Float_t dZ = s2->Z() - s1->Z();
  Float_t TX = (s2->X() - s1->X())/dZ;
  Float_t TY = (s2->Y() - s1->Y())/dZ;
  Float_t dTX1 = s1->TX() - TX;
  Float_t dTX2 = s2->TX() - TX;
  Float_t dTY1 = s1->TY() - TY;
  Float_t dTY2 = s2->TY() - TY;
  Float_t restx = c->SigmaTX(TX);
  Float_t restx2 = restx*restx;
  Float_t resty = c->SigmaTY(TY);
  Float_t resty2 = resty*resty;
  Float_t prob1 = c->ProbSeg(TX, TY, s1->W());
  Float_t prob2 = c->ProbSeg(TX, TY, s2->W());
  Float_t chi2 = TMath::Sqrt((dTX1*dTX1)/restx2/prob1 + (dTX2*dTX2)/restx2/prob2 +
                             (dTY1*dTY1)/resty2/prob1 + (dTY2*dTY2)/resty2/prob2)/2; /// svt up - sqrt and prob1, prob2 were added
  return chi2;
}
//-----------------------------------------------------------
Float_t Prob(EdbSegP* s1, EdbSegP* s2, EdbScanCond* c)
{
  s1->SetErrors();
  s2->SetErrors();
  Float_t s1TX = s1->TX();
  Float_t s2TX = s2->TX();
  Float_t s1TY = s1->TY();
  Float_t s2TY = s2->TY();
  c->FillErrorsCov(s1TX, s1TY, s1->COV());
  c->FillErrorsCov(s2TX, s2TY, s2->COV());
  Float_t dz = s2->Z() - s1->Z();
  Float_t tx = (s2->X() - s1->X())/dz;
  Float_t ty = (s2->Y() - s1->Y())/dz;
  Float_t dtx1 = (s1TX - tx)*(s1TX - tx)/s1->STX();
  Float_t dty1 = (s1TY - ty)*(s1TY - ty)/s1->STY();
  Float_t dtx2 = (s2TX - tx)*(s2TX - tx)/s2->STX();
  Float_t dty2 = (s2TY - ty)*(s2TY - ty)/s2->STY();
  Float_t chi2 = TMath::Sqrt(dtx1 + dty1 + dtx2 + dty2);
  Float_t prob = TMath::Prob(chi2*chi2, 4);
  s1->SetProb(c->ProbSeg(tx, ty, s1->W()));
  s2->SetProb(c->ProbSeg(tx, ty, s2->W()));
  return s1->Prob()*s2->Prob()*prob;
}
//-----------------------------------------------------------
void FormCouple(Int_t id, EdbSegP* s1, EdbSegP* s2, EdbScanCond* c, EdbSegP* s)
{
  Float_t PHsum = s1->W() + s2->W();
  Float_t dZ = (s2->Z() - s1->Z());
  Float_t TX = (s2->X() - s1->X())/dZ;
  Float_t TY = (s2->Y() - s1->Y())/dZ;
  Float_t X = (s2->X() + s1->X())/2;
  Float_t Y = (s2->Y() + s1->Y())/2;
  s->Set(id, X, Y, TX, TY, PHsum, 0);
  s->SetZ((s1->Z() + s2->Z())/2);
  s->SetChi2(CalcChi2(s1, s2, c));
  return;
}
//-------------------------------------------------------------------------------
int main(){
	TFile f("FASERnu1.root");
	TTree *t = (TTree*)f.Get("FASERnu");
	TBranch *b = t->FindBranch("row_wise_branch");
	TLeaf *leaf[10] = {b->FindLeaf("x"),b->FindLeaf("y"),b->FindLeaf("z"),b->FindLeaf("px"),b->FindLeaf("py"),b->FindLeaf("pz"),b->FindLeaf("pdgid"),b->FindLeaf("charge"),b->FindLeaf("iz"),b->FindLeaf("izsub")};
	gSystem->mkdir("data", kTRUE);
	float ztable1[101],ztable2[101];
    Long64_t nentries = t->GetEntries(); // temp
    for(Long64_t i=0;i<nentries;i++){
	if(i%1000==0)printf("%d\n",i);
	t->GetEntry(i);
	int nhits = leaf[0]->GetLen();
	for(int j=0;j<nhits;j++){
		double z = leaf[2]->GetValue(j);
		int iz = leaf[8]->GetValue(j);
		int izsub = leaf[9]->GetValue(j);
		if(izsub==0)ztable1[iz] = z;
		else ztable2[iz] = z;
	}
    }
	for(int i=0;i<101;i++){
		cout << "(" << ztable1[i] << "," << ztable2[i] << ")" << endl;
	}
	for(int plate=72;plate<101;plate++){
    printf("plate %d\n",plate);
    TFile file_cp(Form("data/%02d_001.cp.root", plate), "recreate");
    TTree* tree_cp = new TTree("couples", "segments");
    // ----------------------- WRITE OUTPUT --------------------------------
    Int_t pid1 = 0, pid2 = 0;
    Float_t xv = 0, yv = 0;
    EdbSegCouple* cp = 0;
    EdbSegP* edbs1 = 0;
    EdbSegP* edbs2 = 0;
    EdbSegP* edbs = 0;
    tree_cp->Branch("pid1", &pid1, "pid1/I");
    tree_cp->Branch("pid2", &pid2, "pid2/I");
    tree_cp->Branch("xv", &xv, "xv/F");
    tree_cp->Branch("yv", &yv, "yv/F");
    tree_cp->Branch("cp", "EdbSegCouple", &cp, 32000, 99);
    tree_cp->Branch("s1.", "EdbSegP", &edbs1, 32000, 99);
    tree_cp->Branch("s2.", "EdbSegP", &edbs2, 32000, 99);
    tree_cp->Branch("s.", "EdbSegP", &edbs, 32000, 99);
    Float_t eCHI2P;
    EdbScanCond* cond = new EdbScanCond;
    EdbPatCouple pc;
    pc.SetCond(cond);
    for (Long64_t i = 0; i < nentries; i++){
	if(i%100==0)printf("entry %d\n",i);
	t->GetEntry(i);
	int nhits = leaf[0]->GetLen();
	for(int j=0;j<nhits;j++){ // Loop over all hits in event i
		double vals[10] = {1000*leaf[0]->GetValue(j),1000*leaf[1]->GetValue(j),1000*leaf[2]->GetValue(j),leaf[3]->GetValue(j),leaf[4]->GetValue(j),leaf[5]->GetValue(j),leaf[6]->GetValue(j),leaf[7]->GetValue(j),leaf[8]->GetValue(j),leaf[9]->GetValue(j)};
		if(vals[8]!=plate)continue;
		if(vals[9]!=0)continue;
		if(vals[0]<-5000)continue;
		if(vals[0]>5000)continue;
		if(vals[1]<-5000)continue;
		if(vals[1]>5000)continue;
      edbs->Set(34,vals[0],vals[1],vals[3]/vals[5],vals[4]/vals[5],29,0); // isg,x,y,tx,ty,ph1+ph2,0
      edbs->SetZ(vals[2]); // (z1+z2)/2
      edbs->SetPlate(vals[8]); // ipl
      double dz = ztable2[static_cast<int>(vals[8])]-ztable1[static_cast<int>(vals[8])];
      double tx = vals[3]/vals[5];
      double ty = vals[4]/vals[5];
      EdbSegP es1(79,vals[0],vals[1],tx,ty,31,0); // isg,x1,y1,tx1,ty1,ph1/10000,0
      EdbSegP es2(65,vals[0]+tx*dz,vals[1]+ty*dz,tx,ty,31,0); // isg,x1,y1,tx1,ty1,ph1/10000,0
      edbs1 = &es1;
      edbs2 = &es2;
      edbs1->SetSide(1); // top
      edbs2->SetSide(2); // bot
//      edbs1->SetZ(z1);
      edbs1->SetZ(-125);
//      edbs2->SetZ(z2);
      edbs2->SetZ(125);
      cp->SetN1(1);
      cp->SetN2(1);
      FormCouple(tree_cp->GetEntries(), edbs1, edbs2, cond, edbs);
//      edbs1->SetVolume(ph1 % 10000);
      edbs1->SetVolume(12 % 10000);
      edbs2->SetVolume(17 % 10000);
      edbs->SetVolume(12 % 10000 + 17 % 10000);
      edbs->SetProb(Prob(edbs1, edbs2, cond));
      edbs->SetID(vals[6]);
      cp->SetCHI2P(Chi2P(edbs1, edbs2, cond));
      // store couples in tree_cp.
      tree_cp->Fill();
    }}
//    delete file_bt;
    tree_cp->SetAlias("dTX1", "s1.eTX-s.eTX");
    tree_cp->SetAlias("dTY1", "s1.eTY-s.eTY");
    tree_cp->SetAlias("dTX2", "s2.eTX-s.eTX");
    tree_cp->SetAlias("dTY2", "s2.eTY-s.eTY");
    tree_cp->Write();
    file_cp.Close();
    } 
 return 0;
}
//-----------------------------------------------------------
