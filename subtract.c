#include "TFile.h"
#include "TNtuple.h"
#include "TLeaf.h"
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <iterator>
#include <map>
void subtract(TString cpinput,TString linkinput,TString output){
	TFile f(cpinput);
	TNtuple *nt = (TNtuple*)f.Get("ntuple");
	TLeaf *l = nt->FindLeaf("id");
	int nentries = nt->GetEntries();
	std::cout << nentries << std::endl;
	int izlast = 0;
	vector<int> idxvec,idxveclink; // Vector that stores index of first BT in each plate
	for(int i=0;i<nentries;i++){
		nt->GetEntry(i);
		if(izlast<l->GetValue(1)){
			izlast = l->GetValue(1);
			idxvec.push_back(i);
			std::cout << "(" << i << "," << l->GetValue(0) << "," << l->GetValue(1) << ")";
		}
	}
	std::cout << std::endl;
	idxvec.push_back(nentries);
	for(int i=0;i<idxvec.size();i++){
		std::cout << idxvec[i] << " ";
	}
	std::cout << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    TFile g(linkinput);
    TNtuple *ntlink = (TNtuple*)g.Get("ntuple");
    TLeaf *llink = ntlink->FindLeaf("id");
    int nentrieslink = ntlink->GetEntries();
    int izlastlink = 0;
    for(int i=0;i<nentrieslink;i++){
	ntlink->GetEntry(i);
	if(izlastlink<llink->GetValue(1)){
	    izlastlink = llink->GetValue(1);
	    idxveclink.push_back(i);
	}
    }
    idxveclink.push_back(nentrieslink);
    for(int i=0;i<idxveclink.size();i++){
	cout << idxveclink[i] << " ";
    }
    cout << endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    TFile *h = TFile::Open(output,"RECREATE"); 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    for(int layer=0;layer<29;layer++){ // The layer for which we make the map
        map<int, vector<double>> btmap;    // empty map container
        for(int i=idxvec[layer];i<idxvec[layer+1];i++){
            nt->GetEntry(i);
            btmap.insert(pair<int, vector<double>>(l->GetValue(0),{l->GetValue(1),l->GetValue(2),l->GetValue(3),l->GetValue(4),l->GetValue(5),l->GetValue(6),l->GetValue(7),l->GetValue(8),l->GetValue(9)}));
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Erase from this map the BTs belonging to the first layer of penetrating BTs
	int num;
	for(int i=idxveclink[layer];i<idxveclink[layer+1];i++){
	    ntlink->GetEntry(i);
	    num = btmap.erase(llink->GetValue(0));
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // printing btmap
	vector<vector<double>> btvec;
	vector<double> bt;
	map<int, vector<double>>::iterator itr;
	for (itr = btmap.begin(); itr != btmap.end(); ++itr) {
	    for(int i=0;i<10;i++){
		if(i==0)bt.push_back(itr->first);
		else bt.push_back(itr->second[i-1]);
            }
	    btvec.push_back(bt);
	    bt.clear();
        }
        cout << "btvec.size() = " << btvec.size() << endl;
	TString arrname;
	arrname.Form("arr%d",90+layer);
	h->WriteObject(&btvec,arrname);
    }
}
